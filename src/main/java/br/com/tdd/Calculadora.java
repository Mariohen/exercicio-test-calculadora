package br.com.tdd;

import java.math.BigDecimal;

public class Calculadora {

    //Pesquisar para trabalhar com BigDecimal
        public static int soma (int primeiroNumero, int segundoNumero){
            int resultado = primeiroNumero + segundoNumero;
            return resultado;
        }

    public static double soma (double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public static int subtracao(int primeiroNumero, int segundoNumero) {
            int resultado = primeiroNumero - segundoNumero;
            return resultado;
    }

    public static int subtracaoNegativos(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero - segundoNumero;
        return resultado;
    }

    public static double subtracao(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero - segundoNumero;
        return resultado;
    }

    public static double subtracaoNegativos(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero - segundoNumero;
        return resultado;
    }

    public static int divisao(int primeiroNumero, int segundoNumero) {
            int resultado = primeiroNumero / segundoNumero;
            return resultado;
    }

    public static double divisao(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero / segundoNumero;
        return resultado;
    }


    public static int multiplicacaoNumerosInteiros(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
     }
    public static double multiplicacaoNumerosReais(double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero * segundoNumero;
        return resultado;
    }


}


