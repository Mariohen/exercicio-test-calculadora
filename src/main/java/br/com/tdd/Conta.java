package br.com.tdd;

public class Conta {

    private Cliente cLiente;
    private double saldo;

    public Conta(Cliente cLiente, double saldo) {
        this.cLiente = cLiente;
        this.saldo = saldo;
    }

    public Cliente getcLiente() {
        return cLiente;
    }

    public void setcLiente(Cliente cLiente) {
        this.cLiente = cLiente;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void depositar(double valor){
        this.saldo = this.saldo + valor;
    }

    public void sacar(double valor){
        if (valor > this.saldo){
            throw new RuntimeException("Saldo Insulficiente");
        }
        this.saldo = this.saldo - valor;

    }
}
