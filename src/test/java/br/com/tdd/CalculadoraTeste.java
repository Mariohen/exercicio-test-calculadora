package br.com.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class CalculadoraTeste {

    @Test       //Anotacao do Jnuit de testes
    public void testarSomardeDoisNumerosInteiros(){
        int resultado = Calculadora.soma(2,2);

        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarSomardeDoisNumerosReais(){
        double resultado = Calculadora.soma(2.02,2.02);
      Assertions.assertEquals(4.04, resultado);
    }

    //******************************************
    @Test
    public void testarSubtracaoNumerosInteiros(){
        int resultado = Calculadora.subtracao(4,2);
        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void testarSubtracaoInteiros(){
        int resultado = Calculadora.subtracaoNegativos(2,4);
        Assertions.assertEquals(-2, resultado);
    }

    @Test
    public void testarSubtracaoNumerosReais(){
        double resultado = Calculadora.subtracao(4.04,2.02);
        Assertions.assertEquals(2.02, resultado);
    }

    @Test
    public void testarSubtracaoInteirosReais(){
        double resultado = Calculadora.subtracaoNegativos(2.05,4.02);
        Assertions.assertEquals(-6.07, resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosInteiros (){
        int resultado = Calculadora.multiplicacaoNumerosInteiros(2, 2);
        Assertions.assertEquals(4, resultado);
    }

    public void testarMultiplicacaoDeDoisNumerosReais (){
        double resultado = Calculadora.multiplicacaoNumerosReais(2.02, 2.04);
        Assertions.assertEquals(4.08, resultado);
    }

    @Test       //Anotacao do Jnuit de testes
    public void testarDivisaoDoisNumerosInteiros(){
        int resultado = Calculadora.divisao(4,2);

        Assertions.assertEquals(2, resultado);
    }

    @Test       //Anotacao do Jnuit de testes
    public void testarDivisaoDoisNumerosReais(){
        double resultado = Calculadora.divisao(4.30,2);

        Assertions.assertEquals(2.15, resultado);
    }

}
