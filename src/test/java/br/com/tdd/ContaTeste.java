package br.com.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ContaTeste {

    private Cliente cliente;
    private Conta conta;

    @BeforeEach
    public void setup(){
        this.cliente = new Cliente("Mario");
        this.conta = new Conta(this.cliente, 100.00);
    }

    @Test
    public void testarDepositoEmConta(){
        Cliente cliente = new Cliente("Mario");
        Conta conta = new Conta(cliente, 100.00);
        double valorDeDeposito = 400.00;

        conta.depositar(valorDeDeposito);

        Assertions.assertEquals(500.00, conta.getSaldo());
    }

    @Test
    public void testarSaqueDeConta(){
        double valorDeDeposito = 400.00;

        conta.sacar(valorDeDeposito);

        Assertions.assertEquals(50.00, conta.getSaldo());
    }

    // Teste com exececao com LAMBDA
    @Test
    public void testarSaqueDeContaSemSaldo(){
        double valorDeSacar = 400.00;
        Assertions.assertThrows(RuntimeException.class, ()-> {conta.sacar(valorDeSacar);});
    }

}


